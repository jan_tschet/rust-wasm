FROM rust:1.40

WORKDIR /usr/src/myapp

RUN apt-get update && apt-get install -y rsync curl bash gcc g++ make
RUN curl -sL https://deb.nodesource.com/setup_13.x | bash -
RUN apt-get install -y nodejs
RUN curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
EXPOSE 80
